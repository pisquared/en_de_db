#!/bin/bash
SONG_NAME=$1
RUN_ONLY=$2
source venv/bin/activate
export GOOGLE_APPLICATION_CREDENTIALS="pi2-tts-svc-acc.json"
if [[ ${RUN_ONLY} == "lyrics" ]]; then
    python lyrics_time.py ${SONG_NAME}
elif [[ ${RUN_ONLY} == "generate" ]]; then
    python generate_song.py ${SONG_NAME}
else
    python lyrics_time.py ${SONG_NAME}
    python generate_song.py ${SONG_NAME}
fi