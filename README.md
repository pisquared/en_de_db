# en_de_db

Generate learning de->en audio from song lyrics.

## install

```
./INSTALL.sh
```

# Run for a new song

1. You first need a service account secret from [Google Cloud Text-To-Speech API](https://cloud.google.com/text-to-speech/docs/quickstart-client-libraries#client-libraries-install-python) (go through the steps of `Before you begin` section, the rest is handled by the app).

2. Then for each song, you need:
- `songs/<song_name>/<song_name>.lyrics.txt` -> lyrics of the song
- `songs/<song_name>/<song_name>.mp3` -> the song
- Which will generate output in `generated/<song_name>.mp3`

3. Running will additionally generate:
- `songs/<song_name>/<song_name>.timings.txt` -> how lyrics correspond to time
- `songs/<song_name>/<song_name>.translations.csv` -> translations of lyrics
- `songs/<song_name>/<song_name>.preaudio.txt` -> intermediate file for generating the final mp3
- `sounds/<number>_<lang>.mp3` - audio snippets of spoken words/phrases

To run a completely new song:
```
./RUN.sh <song_name>
```

This will first ask you to time the lyrics, pressing enter when the previous lyrics line finish and the new one are about to start.

Then the program will generate final output in `generated/<song_name>.mp3`

## Re-Run for existing song

To re-do the lyrics timings:
```bash
./RUN.sh <song_name> lyrics
```

To re-generate song from timings:
```bash
./RUN.sh <song_name> generate
```