Ich will der Allerbeste sein,
wie keiner vor mir war.
Ganz allein fang' ich sie mir,
ich kenne die Gefahr.
Ich streife durch das ganze Land,
ich suche weit und breit
das Pokémon, um zu versteh'n,
was ihm diese Macht verleiht.
Pokémon,
komm' und schnapp' sie dir,
nur ich und du,
in allem, was ich auch tu',
Pokémon,
du, mein bester Freund,
komm', retten wir die Welt.
Pokémon,
komm' und schnapp' sie dir,
dein Herz ist gut,
wir vertrauen auf uns'ren Mut,
ich lern' von dir und du von mir,
Pokémon.
Komm', schnapp' sie dir,
komm' und schnapp' sie dir.
Egal, wie schwer mein Weg auch ist,
ich nehme es in Kauf.
Ich will den Platz, der mir gehört.
Ich gebe niemals auf.
Komm', zeigen wir der ganzen Welt,
dass wir Freunde sind.
Gemeinsam zieh'n wir in den Kampf,
das beste Team gewinnt.