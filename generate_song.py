import csv
import html
import os
import sys
from string import punctuation

from pydub import AudioSegment
from time import time

from sqlalchemy import Column, Integer
from sqlalchemy import create_engine, Table, ForeignKey, Unicode
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

import tts

LANG_REGIONS = {
    'en': 'en-US',
    'de': 'de-de',
}
SHORT_SILENCE = 100
LONG_SILENCE = 300

db_path = 'words.db'
engine = create_engine('sqlite:///{}'.format(db_path))
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()


def generate_sound(word_id, lang_word, lang):
    lang_region = LANG_REGIONS.get(lang)
    filename = "{}_{}.mp3".format(word_id, lang)
    filepath = os.path.join("sounds", filename)

    start = time()
    tts.gen_speech(lang_word, lang_region, filepath)
    duration = time() - start
    print("  Generated ({} - {:02.2f} s): {}".format(lang, duration, lang_word))

    return filepath


words_labels = Table('words_labels', Base.metadata,
                     Column('word_id', Integer, ForeignKey('word.id')),
                     Column('label_id', Integer, ForeignKey('label.id'))
                     )


class Label(Base):
    __tablename__ = 'label'

    id = Column(Integer, primary_key=True)
    name = Column(Unicode)

    words = relationship("Word", secondary=words_labels, back_populates="labels")

    @classmethod
    def get_or_create(cls, name):
        global session
        label = session.query(cls).filter_by(name=name).first()
        if label:
            return label
        label = cls(
            name=name,
        )
        session.add(label)
        return label


class Word(Base):
    __tablename__ = 'word'

    id = Column(Integer, primary_key=True)

    de = Column(Unicode)
    en = Column(Unicode)

    de_sound = Column(Unicode)
    en_sound = Column(Unicode)

    word_type = Column(Unicode)  # noun, verb, phrase,...
    learn_level = Column(Integer, default=0)  # 0:NEW 1:LOW 2:MEDIUM 3:HIGH 4:WELL_KNOWN

    labels = relationship("Label", secondary=words_labels, back_populates="words")

    @classmethod
    def get_or_create(cls, de, en=None, de_sound=None, en_sound=None,
                      word_type=None, label_names=None, filters=None):
        global session
        if not label_names:
            label_names = []
        if not filters:
            filters = dict(de=de)
        existing_word = session.query(cls).filter_by(**filters).first()
        if not existing_word:
            existing_word = cls(
                de=de,
                en=en,
                de_sound=de_sound,
                en_sound=en_sound,
                word_type=word_type,
            )
            session.add(existing_word)
            session.commit()
            for label_name in label_names:
                label_name = label_name.strip()
                if not label_name:
                    continue
                label = Label.get_or_create(
                    name=label_name,
                )
                existing_word.labels.append(label)
        else:
            if word_type and not existing_word.word_type:
                existing_word.word_type = word_type
            for label_name in label_names:
                label_name = label_name.strip()
                if not label_name:
                    continue
                label = Label.get_or_create(
                    name=label_name,
                )
                if label not in existing_word.labels:
                    existing_word.labels.append(label)
        for lang in LANG_REGIONS.keys():
            if not getattr(existing_word, "{}_sound".format(lang)):
                lang_word = getattr(existing_word, lang)
                filepath = generate_sound(existing_word.id, lang_word, lang)
                setattr(existing_word, "{}_sound".format(lang), filepath)
        session.add(existing_word)
        session.commit()
        return existing_word

    @classmethod
    def get_one_by(cls, **filters):
        return session.query(cls).filter_by(**filters).first()


class Translation(object):
    def __init__(self, de, tr_type, en=None):
        self.de = de
        self.tr_type = tr_type
        self.en = en


class Timing(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def to_list(self):
        return ["#", self.start, self.end]


class Translations(object):
    def __init__(self):
        self._translations = []
        self.timings = []
        self.results = []

    def add(self, translation, second=False):
        if not translation.en and not second:
            self._translations.append(translation)
        self.results.append(translation)

    def add_timing(self, this_start):
        timing = Timing(this_start, this_start)
        # First timing
        if self.timings:
            self.timings[-1].end = this_start
        self.results.append(timing)
        self.timings.append(timing)
        if len(self.timings) > 2:
            self.timings[-1].start = self.timings[-3].end

    def translate(self):
        SEGMENT_SIZE = 100
        from google.cloud import translate
        translate_client = translate.Client()

        for start in range(0, len(self._translations), SEGMENT_SIZE):
            end = start + SEGMENT_SIZE
            segment = [t.de for t in self._translations[start:end]]
            segment_translations = translate_client.translate(segment, source_language='de', target_language='en')

            for t in segment_translations:
                de = str(t['input'])
                en = html.unescape(str(t['translatedText']))
                Word.get_or_create(de=de, en=en)


def add_element(element, tr_type, translations, second=False):
    translation = Translation(de=element, tr_type=tr_type)
    existing_word = Word.get_one_by(de=element)
    if existing_word:
        translation.en = existing_word.en
    translations.add(translation, second)


def from_song(song_name):
    cache = set()
    translations = Translations()
    with open('songs/{song_name}/{song_name}.timings.txt'.format(song_name=song_name)) as f_tr:
        for line in f_tr.readlines():
            timing = None
            line = line.strip()
            if '|' in line:
                timing, line = line.split('|')
            if not line:
                continue
            line = ''.join(c for c in line if c not in punctuation)
            if line not in cache:
                if timing:
                    translations.add_timing(timing)
                add_element(line, "phrase_1", translations)
            for word in line.split():
                if word.lower() not in cache:
                    cache.add(word.lower())
                    add_element(word, "word", translations)
            if line not in cache:
                cache.add(line)
                add_element(line, "phrase_2", translations, True)
    translations.translate()
    tr_csv_filepath = 'songs/{song_name}/{song_name}.translations.csv'.format(song_name=song_name)
    prea_csv_filepath = 'songs/{song_name}/{song_name}.preaudio.csv'.format(song_name=song_name)
    if not os.path.isfile(tr_csv_filepath):
        os.mknod(tr_csv_filepath)
    if not os.path.isfile(prea_csv_filepath):
        os.mknod(prea_csv_filepath)
    with open(tr_csv_filepath, 'w') as f_tr, open(prea_csv_filepath, 'w') as f_prea:
        tr_writer = csv.writer(f_tr)
        prea_writer = csv.writer(f_prea)
        for result in translations.results:
            if type(result) is Timing:
                prea_writer.writerow(result.to_list())
            else:
                word_type = result.tr_type if result.tr_type == "word" else "phrase"
                word = Word.get_or_create(de=result.de, en=result.en,
                                          word_type=word_type, label_names=[song_name])
                tr_writer.writerow([word.id, result.tr_type, word.learn_level, word.de, word.en])
                prea_writer.writerow([word.id, result.tr_type, word.learn_level, word.de, word.en])


def time_to_ms(ttime):
    m, s = ttime.split(":")
    return int(60 * 1000 * int(m) + 1000 * float(s))


def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


def song_to_audio(song_name):
    mp3_filepath = 'songs/{song_name}/{song_name}.mp3'.format(song_name=song_name)
    csv_filepath = 'songs/{song_name}/{song_name}.preaudio.csv'.format(song_name=song_name)
    exp_filepath = 'generated/{song_name}.mp3'.format(song_name=song_name)

    song_audio = AudioSegment.from_mp3(mp3_filepath)
    generated = AudioSegment.silent(duration=1)
    silence = AudioSegment.silent(duration=SHORT_SILENCE)
    long_silence = AudioSegment.silent(duration=LONG_SILENCE)
    current_line, linecount = 0, file_len(csv_filepath)
    with open(csv_filepath) as f:
        reader = csv.reader(f)
        initial_start = time()
        for row in reader:
            current_line += 1
            row_type, arg1, arg2 = row[0], row[1], row[2]
            start = time()
            if row_type == '#':
                method = "song"
                word_type = ''
                start_time, end_time = arg1, arg2
                start_ms, end_ms = time_to_ms(start_time), time_to_ms(end_time)
                generated = generated + song_audio[start_ms:end_ms]
            else:
                method = "word"
                word_id = row_type
                word_type = arg1
                word_learn_level = arg2
                word, changed = Word.get_one_by(id=word_id), False
                if not word.de_sound:
                    filepath = generate_sound(word_id, word.de, 'de')
                    word.de_sound = filepath
                    session.add(word)
                    changed = True
                sound_de = AudioSegment.from_mp3(word.de_sound)
                if not word.en_sound:
                    filepath = generate_sound(word_id, word.en, 'en')
                    word.en_sound = filepath
                    session.add(word)
                    changed = True
                sound_en = AudioSegment.from_mp3(word.en_sound)
                if changed:
                    session.commit()

                if word_type == "phrase_1":
                    generated = generated + sound_de + silence + sound_en + long_silence
                elif word_type == "phrase_2":
                    generated = generated + sound_de + long_silence
                elif word_learn_level in ["0", "1"]:
                    generated = generated + sound_de + silence + sound_en + silence + sound_de + long_silence
                elif word_learn_level in ["2"]:
                    generated = generated + sound_de + silence + sound_en + long_silence
                # skip 3

            duration = time() - start
            elapsed = time() - initial_start
            eta = ((elapsed * linecount) / current_line) - elapsed
            print("{}/{}: {} {} - {:02.2f} s| Elapsed: {:02.2f} | ETA: {:02.2f}".format(
                current_line, linecount, method, word_type, duration, elapsed, eta))

    generated.export(exp_filepath, format="mp3")


def main():
    global session
    if not os.path.isfile(db_path):
        Base.metadata.create_all(engine)
    tts.init_tts()


if __name__ == "__main__":
    main()
    song = sys.argv[1]
    from_song(song)
    song_to_audio(song)
