import csv
import os
import sys
from time import time

from pydub import AudioSegment

from generate_song import main, Word, Label, session, SHORT_SILENCE, LONG_SILENCE, LANG_REGIONS, generate_sound


# äöü
# ÄÖÜ


def import_from_csv():
    with open("words.csv", 'r') as csvFile:
        reader = csv.reader(csvFile)
        for row in reader:
            de, en, word_type = row[:3]
            label_names = row[3:]
            Word.get_or_create(
                de=de, en=en,
                word_type=word_type,
                label_names=label_names,
                filters=dict(de=de)
            )


def compile_from_label(label_name):
    generated = AudioSegment.silent(duration=1)
    silence = AudioSegment.silent(duration=SHORT_SILENCE)
    long_silence = AudioSegment.silent(duration=LONG_SILENCE)

    label = session.query(Label).filter_by(name=label_name).first()
    initial_start = time()

    words = label.words
    linecount = len(words)
    for i, word in enumerate(words):
        start = time()
        for lang in LANG_REGIONS.keys():
            if not getattr(word, "{}_sound".format(lang)):
                lang_word = getattr(word, lang)
                filepath = generate_sound(word.id, lang_word, lang)
                setattr(word, "{}_sound".format(lang), filepath)
            else:
                if not os.path.isfile(getattr(word, "{}_sound".format(lang))):
                    lang_word = getattr(word, lang)
                    generate_sound(word.id, lang_word, lang)
        sound_en = AudioSegment.from_mp3(word.en_sound)
        sound_de = AudioSegment.from_mp3(word.de_sound)
        generated = generated + sound_de + silence + sound_en + silence + sound_de + long_silence

        duration = time() - start
        elapsed = time() - initial_start
        eta = ((elapsed * linecount) / (i + 1)) - elapsed
        print("{}/{} - {:02.2f} s| Elapsed: {:02.2f} | ETA: {:02.2f} | {} - {}".format(
            i, linecount, duration, elapsed, eta, word.de, word.de_sound))

    exp_filepath = 'generated/{filename}.mp3'.format(filename=label_name)
    generated.export(exp_filepath, format="mp3")


# def delete_all_sounds():
#     words = session.query(Word).all()
#     for word in words:
#         word.en_sound = None
# word.de_sound = None
# session.add(word)
# session.commit()
# for word in words:
#     Word.get_or_create(word.de, filters={"id": word.id})


if __name__ == "__main__":
    main()
    label = sys.argv[1]
    # import_from_csv()
    compile_from_label(label)
