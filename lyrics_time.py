import os
import sys
from time import time

timings = []
final_lines = []


def sec_to_human(timing):
    minutes = timing // 60
    seconds = timing - minutes * 60
    return "{:02.0f}:{:05.2f}".format(minutes, seconds)


def time_to_s(ttime):
    m, s = ttime.split(":")
    return 60 * int(m) + float(s)


def main(song):
    with open('songs/{}/{}.lyrics.txt'.format(song, song)) as f:
        try:
            input('! press enter to start...')
            print('! start')
            start = time()
            frozens = 0
            from_correction = False
            while True:
                try:
                    if from_correction:
                        from_correction = False
                    else:
                        next_line = f.readline().strip()
                except Exception:
                    next_line = "-----"
                c = input("> ")
                new_time = time()
                if c == 'c':
                    # correction, freeze_time
                    frozen_time = timings[-1]
                    frozen_time_human = sec_to_human(frozen_time)
                    print("! FROZEN TIME: {}".format(frozen_time_human))
                    while True:
                        correct_time = input("> correct: ")
                        seconds = time_to_s(correct_time)
                        timings[-1] = seconds
                        look_right = input("> {} look right?".format(sec_to_human(seconds)))
                        if look_right == 'y':
                            break
                    input("! Now go to {} and start playing. Press enter when ready".format(frozen_time_human))
                    frozens += time() - new_time
                    from_correction = True
                    continue
                new_time_diff = new_time - start - frozens
                timings.append(new_time_diff)
                final_lines.append(next_line)
                print("{}|{}".format(sec_to_human(new_time_diff), next_line))

        except KeyboardInterrupt as e:
            print(e)
            print("===========")
            finished = False
            correction = 0
            while True:
                try:
                    print()
                    correction = float(input("Enter correction in seconds here:"))
                    for i, timing in enumerate(timings):
                        print("{}|{}".format(sec_to_human(timing + correction), final_lines[i]))
                    print()
                    look_right = input("> {} look right?")
                    if look_right == 'y':
                        finished = True
                        break
                except Exception:
                    continue
                except KeyboardInterrupt as e:
                    break
                if finished:
                    break
    timings_filepath = 'songs/{song_name}/{song_name}.timings.txt'.format(song_name=song)
    if not os.path.isfile(timings_filepath):
        os.mknod(timings_filepath)
    with open(timings_filepath, 'w') as f:
        for i, timing in enumerate(timings):
            f.write("{}|{}\n".format(sec_to_human(timing + correction), final_lines[i]))


if __name__ == '__main__':
    song = sys.argv[1]
    main(song)
